import os
import time

from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode

application = Flask(__name__)
app = application


def get_db_creds():
    db = os.environ.get("DB", None) or os.environ.get("database", None)
    username = os.environ.get("USER", None) or os.environ.get("username", None)
    password = os.environ.get("PASSWORD", None) or os.environ.get("password", None)
    hostname = os.environ.get("HOST", None) or os.environ.get("dbhost", None)
    return db, username, password, hostname

def create_table():
    # Check if table exists or not. Create it only if it does not exist.
    db, username, password, hostname = get_db_creds()
    table_ddl = 'CREATE TABLE movies(id INT UNSIGNED NOT NULL AUTO_INCREMENT, year VARCHAR(32), title VARCHAR(255), director VARCHAR(255), actor VARCHAR(255), release_date VARCHAR(32), rating FLOAT, PRIMARY KEY (id))'
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
    cur = cnx.cursor()
    try:
        cur.execute(table_ddl)
        cnx.commit()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
            # Clear the table before grading; TRUNCATE causes an implicit commit
            print("clearing table for grading...")
            cur.execute("TRUNCATE TABLE movies;")
        else:
            print(err.msg)

try:
    print("---------" + time.strftime('%a %H:%M:%S'))
    print("Before create_table")
    create_table()
    print("After create_table")
except Exception as exp:
    print("Got exception %s" % exp)
    conn = None

# app routes start here

@app.route('/delete_movie', methods=['POST'])
def delete_movie():
    db, username, password, hostname = get_db_creds()
    print("Inside delete_movie")
    title = request.form['delete_title']
    print(title)
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
    cur = cnx.cursor()
    sql_query = "DELETE FROM movies WHERE LOWER(title) = LOWER(%s);"
    query_data = (title,)
    try:
        cur.execute("SELECT * FROM movies WHERE LOWER(title) = LOWER(%s);", (title,))
        if not cur.fetchall():
            return render_template('index.html', message="Movie with " + title + " does not exist")
        cur.execute(sql_query, query_data)
        cnx.commit()
        return render_template('index.html', message="Movie " + title + " successfully deleted")
    except Exception as e:        
        print(e)
        return render_template('index.html', message="Movie "+ title +" could not be deleted - "+str(e))

@app.route('/search_movie', methods=['GET'])
def search_movie():
    db, username, password, hostname = get_db_creds()
    print("Inside search_movie")
    actor = request.args.get('search_actor')
    print(actor)
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
    cur = cnx.cursor()
    sql_query = "SELECT title, year, actor FROM movies WHERE LOWER(%s) = LOWER(actor);"
    query_data = (actor,)
    try:
        cur.execute(sql_query, query_data)
    except Exception as e:        
        print(e)
        return render_template('index.html', message="Actor "+actor+" could not be found - "+str(e))
    entries = ""
    rows = cur.fetchall()
    if len(rows) == 0:
        return render_template('index.html', message="No movies found for actor " + actor)
    for row in rows:
        entries = entries + ", ".join(row) + '<br>' 
    return render_template('index.html', message=entries)

@app.route('/add_movie', methods=['POST'])
def add_movie():
    print("Received ADD MOVIE request.")
    print(request.form['title'])
    year = request.form['year']
    title = request.form['title']
    director = request.form['director']
    actor = request.form['actor']
    release_date = request.form['release_date']
    rating = request.form['rating']
    db, username, password, hostname = get_db_creds()
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
    cur = cnx.cursor()
    sql_query = "INSERT INTO movies (year,title,director,actor,release_date,rating) values (%s,%s,%s,%s,%s,%s);"
    query_data = (year,title,director,actor,release_date,rating)
    try:
        cur.execute("SELECT * FROM movies WHERE LOWER(title) = LOWER(%s);", (title,))
        if cur.fetchall():
            return render_template('index.html', message="Movie " + title + " could not be inserted - movie already exists")
        cur.execute(sql_query, query_data)
        cnx.commit()
        return render_template('index.html', message="Movie " + title + " successfully inserted")
    except Exception as e:
        print(e)
        return render_template('index.html', message="Movie "+title+" could not be inserted - "+str(e))

@app.route('/update_movie', methods=['POST'])
def update_movie():
    print("Received UPDATE MOVIE request.")
    print(request.form['title'])
    year = request.form['year']
    title = request.form['title']
    director = request.form['director']
    actor = request.form['actor']
    release_date = request.form['release_date']
    rating = request.form['rating']
    db, username, password, hostname = get_db_creds()
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
    cur = cnx.cursor()
    sql_query = "UPDATE movies SET year = %s, title = %s, director = %s, actor = %s, release_date = %s, rating = %s WHERE LOWER(title) = LOWER(%s);"
    query_data = (year,title,director,actor,release_date,rating,title)
    try:
        cur.execute("SELECT * FROM movies WHERE LOWER(title) = LOWER(%s);", (title,))
        if not cur.fetchall():
            return render_template('index.html', message="Movie with " + title + " does not exist")
        cur.execute(sql_query, query_data)
        cnx.commit()
        return render_template('index.html', message="Movie " + title + " successfully updated")
    except Exception as e:
        print(e)
        return render_template('index.html', message="Movie "+title+" could not be updated - "+str(e))

@app.route('/highest_rating', methods=['GET'])
def highest_rating():
    db, username, password, hostname = get_db_creds()
    print("Inside highest_rating")
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
    cur = cnx.cursor()
    sql_query = "SELECT title, year, actor, director, rating FROM movies WHERE rating = (SELECT MAX(rating) FROM movies);"
    try:
        cur.execute(sql_query)
    except Exception as e:        
        print(e)
        return render_template('index.html', message="Highest ratings could not be displayed - "+str(e))
    entries = ""
    rows = cur.fetchall()
    for row in rows:
        print(", ".join(str(c) for c in row))
        entries = entries + ", ".join(str(c) for c in row) + '<br>' 
    return render_template('index.html', message=entries)

@app.route('/lowest_rating', methods=['GET'])
def lowest_rating():
    db, username, password, hostname = get_db_creds()
    print("Inside lowest_rating")
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
    cur = cnx.cursor()
    sql_query = "SELECT title, year, actor, director, rating FROM movies WHERE rating = (SELECT MIN(rating) FROM movies);"
    try:
        cur.execute(sql_query)
    except Exception as e:        
        print(e)
        return render_template('index.html', message="Lowest ratings could not be displayed - "+str(e))
    entries = ""
    rows = cur.fetchall()
    for row in rows:
        entries = entries + ", ".join(str(c) for c in row) + '<br>' 
    return render_template('index.html', message=entries)

@app.route("/")
def hello():
    print("Printing available environment variables")
    print(os.environ)
    print("Before displaying index.html")
    return render_template('index.html', message="")


if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')
